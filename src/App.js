import React from 'react';
import logo from './logo.svg';
import './App.css';
import {RULE_NAMES} from './rules';

function App() {
  return (
    <div className="App">
      <Can
        role={user.role}
        perform= {RULE_NAMES.LOADBOARD_EDIT}
        yes={() => (
          <div>
            <h1>Dashboard</h1>
            <Logout />
            <Profile />
            <PostsList />
          </div>
        )}
        no={() => <Redirect to="/" />}
      />
    </div>
  );
}

export default App;
