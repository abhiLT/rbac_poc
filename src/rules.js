const RULE_NAMES: {
  LOADBOARD_VIEW: "loadboard:view",
  LOADBOARD_EDIT: "loadboard:edit",
  PAYMENT_VIEW: 'payment:view',
}
conrt ROLE: {
  FO: of
}
const rules = {
  fo: {
    static: [RULE_NAMES.LOADBOARD_VIEW]
  },
  admin: {
    static: [
      RULE_NAMES.LOADBOARD_VIEW,
      RULE_NAMES.PAYMENT_VIEW
    ],
    dynamic: {
      RULE_NAMES.LOADBOARD_EDIT: ({userId, verfiedID}) => {
        return userId === verfiedID;
      }
    }
  },
};

export default rules;
